webpackHotUpdate("static/development/pages/index.js",{

/***/ "./src/components/NeoButton.js":
/*!*************************************!*\
  !*** ./src/components/NeoButton.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
var _jsxFileName = "/Users/theozanchi/theo/web/neomorphism_test/src/components/NeoButton.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




var NeoButton = function NeoButton(_ref) {
  var children = _ref.children,
      width = _ref.width,
      height = _ref.height;
  return __jsx(ButtonWrapper, {
    width: width,
    height: height,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, children);
};

NeoButton.PropTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node), prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node]).isRequired,
  width: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired,
  height: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (NeoButton);
var ButtonWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2__["default"].button.withConfig({
  displayName: "NeoButton__ButtonWrapper",
  componentId: "sc-1e52b0k-0"
})(["border-radius:20px;background:#feffff;box-shadow:13px 13px 26px #bfbfbf,-13px -13px 26px #ffffff;margin:auto;width:", ";height:", ";border:none;"], function (props) {
  return "".concat(props.width, "rem");
}, function (props) {
  return "".concat(props.height, "rem");
});

/***/ })

})
//# sourceMappingURL=index.js.e8f1cd0491d448211f5f.hot-update.js.map