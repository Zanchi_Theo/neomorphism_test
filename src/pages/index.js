import React from 'react'
import styled from 'styled-components'
import Layout from '../components/Layout'
import NeoButton from '../components/NeoButton'

const Index = () => {
  return (
    <Layout>
      <Container>
        <NeoButton width={6} height={2}>
          TEST
        </NeoButton>
      </Container>
    </Layout>
  )
}

export default Index

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 2rem;
`