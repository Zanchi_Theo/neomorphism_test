import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const NeoButton = ({ children, width, height }) => {
  return(
    <ButtonWrapper width={width} height={height}>
      {children}
    </ButtonWrapper>
  )
}
NeoButton.PropTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
}

export default NeoButton

const ButtonWrapper = styled.button`
  border-radius: 20px;
  background: #feffff;
  box-shadow:  13px 13px 26px #bfbfbf, -13px -13px 26px #ffffff;
  margin: auto;
  width: ${(props) => `${props.width}rem`};
  height: ${(props) => `${props.height}rem`};
`