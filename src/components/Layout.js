import React from 'react'
import PropTypes from 'prop-types'

const Layout = ({ children }) => {
  return (
    <div className="page-layout">
      {children}
      <style jsx global>{`
        body {
          font-size: 10px;
          font-family: sans-serif;
        }
        button {
          border: none;
          cursor: pointer;
        }
      `}</style>
    </div>
  )
}
Layout.PropTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
}

export default Layout
